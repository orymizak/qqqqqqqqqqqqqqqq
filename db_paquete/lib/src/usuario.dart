import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:partida/partida.dart';

class Usuario {
   String usuario;
   int telefono;
   String password;
  final List<Partida> partidas;
  Usuario({
    required this.usuario,
    required this.telefono,
    required this.password,
    required this.partidas,
  });


  Usuario copyWith({
    String? usuario,
    int? telefono,
    String? password,
    List<Partida>? partidas,
  }) {
    return Usuario(
      usuario: usuario ?? this.usuario,
      telefono: telefono ?? this.telefono,
      password: password ?? this.password,
      partidas: partidas ?? this.partidas,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'usuario': usuario,
      'telefono': telefono,
      'password': password,
      'partidas': partidas.map((x) => x.toMap()).toList(),
    };
  }

  factory Usuario.fromMap(Map<String, dynamic> map) {
    return Usuario(
      usuario: map['usuario'],
      telefono: map['telefono'],
      password: map['password'],
      partidas: List<Partida>.from(map['partidas']?.map((x) => Partida.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Usuario.fromJson(String source) => Usuario.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Usuario(usuario: $usuario, telefono: $telefono, password: $password, partidas: $partidas)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;
  
    return other is Usuario &&
      other.usuario == usuario &&
      other.telefono == telefono &&
      other.password == password &&
      listEquals(other.partidas, partidas);
  }

  @override
  int get hashCode {
    return usuario.hashCode ^
      telefono.hashCode ^
      password.hashCode ^
      partidas.hashCode;
  }
}
