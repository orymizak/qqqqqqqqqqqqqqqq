import 'package:db_paquete/db_paquete.dart';
import 'package:partida/partida.dart';

class Evento {}

class Registrar extends Evento {}

class VerificarUsuario extends Evento {}

class CheckRED extends Evento {}

class AgregarNuevaPartida extends Evento {}

class Desconectarse extends Evento {}

class MostrarListas extends Evento {
  final Usuario usuario;

  MostrarListas(this.usuario);
}

class IniciarRonda1 extends Evento {
  final Partida partida;

  IniciarRonda1({required this.partida});
}

class ContinuarRonda2 extends Evento {
  final Partida partida;

  ContinuarRonda2({required this.partida});
}

class ContinuarRonda3 extends Evento {
  final Partida partida;

  ContinuarRonda3({required this.partida});
}

class VerDetallesPartida extends Evento {
  final Partida partida;
  final Usuario usuario;

  VerDetallesPartida({required this.partida, required this.usuario});
}
