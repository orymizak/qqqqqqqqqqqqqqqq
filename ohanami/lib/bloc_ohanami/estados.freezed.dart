// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'estados.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$Estado2TearOff {
  const _$Estado2TearOff();

  Iniciando call() {
    return const Iniciando();
  }

  NuevoRegistro nReg() {
    return const NuevoRegistro();
  }

  ConectadoBDD conB() {
    return const ConectadoBDD();
  }

  DesconectadoBDD desc() {
    return const DesconectadoBDD();
  }

  AgregarPartida agPa() {
    return const AgregarPartida();
  }

  Ronda1 ron1(Partida partida) {
    return Ronda1(
      partida,
    );
  }

  Ronda2 ron2(Partida partida) {
    return Ronda2(
      partida,
    );
  }

  Ronda3 ron3(Partida partida) {
    return Ronda3(
      partida,
    );
  }

  ObtenerListas verL(Usuario usuario) {
    return ObtenerListas(
      usuario,
    );
  }

  DetallesPartida verD(Partida partida, Usuario usuario) {
    return DetallesPartida(
      partida,
      usuario,
    );
  }
}

/// @nodoc
const $Estado2 = _$Estado2TearOff();

/// @nodoc
mixin _$Estado2 {
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $Estado2CopyWith<$Res> {
  factory $Estado2CopyWith(Estado2 value, $Res Function(Estado2) then) =
      _$Estado2CopyWithImpl<$Res>;
}

/// @nodoc
class _$Estado2CopyWithImpl<$Res> implements $Estado2CopyWith<$Res> {
  _$Estado2CopyWithImpl(this._value, this._then);

  final Estado2 _value;
  // ignore: unused_field
  final $Res Function(Estado2) _then;
}

/// @nodoc
abstract class $IniciandoCopyWith<$Res> {
  factory $IniciandoCopyWith(Iniciando value, $Res Function(Iniciando) then) =
      _$IniciandoCopyWithImpl<$Res>;
}

/// @nodoc
class _$IniciandoCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $IniciandoCopyWith<$Res> {
  _$IniciandoCopyWithImpl(Iniciando _value, $Res Function(Iniciando) _then)
      : super(_value, (v) => _then(v as Iniciando));

  @override
  Iniciando get _value => super._value as Iniciando;
}

/// @nodoc

class _$Iniciando implements Iniciando {
  const _$Iniciando();

  @override
  String toString() {
    return 'Estado2()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is Iniciando);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return $default();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return $default?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return $default(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return $default?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(this);
    }
    return orElse();
  }
}

abstract class Iniciando implements Estado2 {
  const factory Iniciando() = _$Iniciando;
}

/// @nodoc
abstract class $NuevoRegistroCopyWith<$Res> {
  factory $NuevoRegistroCopyWith(
          NuevoRegistro value, $Res Function(NuevoRegistro) then) =
      _$NuevoRegistroCopyWithImpl<$Res>;
}

/// @nodoc
class _$NuevoRegistroCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $NuevoRegistroCopyWith<$Res> {
  _$NuevoRegistroCopyWithImpl(
      NuevoRegistro _value, $Res Function(NuevoRegistro) _then)
      : super(_value, (v) => _then(v as NuevoRegistro));

  @override
  NuevoRegistro get _value => super._value as NuevoRegistro;
}

/// @nodoc

class _$NuevoRegistro implements NuevoRegistro {
  const _$NuevoRegistro();

  @override
  String toString() {
    return 'Estado2.nReg()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is NuevoRegistro);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return nReg();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return nReg?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if (nReg != null) {
      return nReg();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return nReg(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return nReg?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if (nReg != null) {
      return nReg(this);
    }
    return orElse();
  }
}

abstract class NuevoRegistro implements Estado2 {
  const factory NuevoRegistro() = _$NuevoRegistro;
}

/// @nodoc
abstract class $ConectadoBDDCopyWith<$Res> {
  factory $ConectadoBDDCopyWith(
          ConectadoBDD value, $Res Function(ConectadoBDD) then) =
      _$ConectadoBDDCopyWithImpl<$Res>;
}

/// @nodoc
class _$ConectadoBDDCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $ConectadoBDDCopyWith<$Res> {
  _$ConectadoBDDCopyWithImpl(
      ConectadoBDD _value, $Res Function(ConectadoBDD) _then)
      : super(_value, (v) => _then(v as ConectadoBDD));

  @override
  ConectadoBDD get _value => super._value as ConectadoBDD;
}

/// @nodoc

class _$ConectadoBDD implements ConectadoBDD {
  const _$ConectadoBDD();

  @override
  String toString() {
    return 'Estado2.conB()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ConectadoBDD);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return conB();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return conB?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if (conB != null) {
      return conB();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return conB(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return conB?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if (conB != null) {
      return conB(this);
    }
    return orElse();
  }
}

abstract class ConectadoBDD implements Estado2 {
  const factory ConectadoBDD() = _$ConectadoBDD;
}

/// @nodoc
abstract class $DesconectadoBDDCopyWith<$Res> {
  factory $DesconectadoBDDCopyWith(
          DesconectadoBDD value, $Res Function(DesconectadoBDD) then) =
      _$DesconectadoBDDCopyWithImpl<$Res>;
}

/// @nodoc
class _$DesconectadoBDDCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $DesconectadoBDDCopyWith<$Res> {
  _$DesconectadoBDDCopyWithImpl(
      DesconectadoBDD _value, $Res Function(DesconectadoBDD) _then)
      : super(_value, (v) => _then(v as DesconectadoBDD));

  @override
  DesconectadoBDD get _value => super._value as DesconectadoBDD;
}

/// @nodoc

class _$DesconectadoBDD implements DesconectadoBDD {
  const _$DesconectadoBDD();

  @override
  String toString() {
    return 'Estado2.desc()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is DesconectadoBDD);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return desc();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return desc?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if (desc != null) {
      return desc();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return desc(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return desc?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if (desc != null) {
      return desc(this);
    }
    return orElse();
  }
}

abstract class DesconectadoBDD implements Estado2 {
  const factory DesconectadoBDD() = _$DesconectadoBDD;
}

/// @nodoc
abstract class $AgregarPartidaCopyWith<$Res> {
  factory $AgregarPartidaCopyWith(
          AgregarPartida value, $Res Function(AgregarPartida) then) =
      _$AgregarPartidaCopyWithImpl<$Res>;
}

/// @nodoc
class _$AgregarPartidaCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $AgregarPartidaCopyWith<$Res> {
  _$AgregarPartidaCopyWithImpl(
      AgregarPartida _value, $Res Function(AgregarPartida) _then)
      : super(_value, (v) => _then(v as AgregarPartida));

  @override
  AgregarPartida get _value => super._value as AgregarPartida;
}

/// @nodoc

class _$AgregarPartida implements AgregarPartida {
  const _$AgregarPartida();

  @override
  String toString() {
    return 'Estado2.agPa()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is AgregarPartida);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return agPa();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return agPa?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if (agPa != null) {
      return agPa();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return agPa(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return agPa?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if (agPa != null) {
      return agPa(this);
    }
    return orElse();
  }
}

abstract class AgregarPartida implements Estado2 {
  const factory AgregarPartida() = _$AgregarPartida;
}

/// @nodoc
abstract class $Ronda1CopyWith<$Res> {
  factory $Ronda1CopyWith(Ronda1 value, $Res Function(Ronda1) then) =
      _$Ronda1CopyWithImpl<$Res>;
  $Res call({Partida partida});
}

/// @nodoc
class _$Ronda1CopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $Ronda1CopyWith<$Res> {
  _$Ronda1CopyWithImpl(Ronda1 _value, $Res Function(Ronda1) _then)
      : super(_value, (v) => _then(v as Ronda1));

  @override
  Ronda1 get _value => super._value as Ronda1;

  @override
  $Res call({
    Object? partida = freezed,
  }) {
    return _then(Ronda1(
      partida == freezed
          ? _value.partida
          : partida // ignore: cast_nullable_to_non_nullable
              as Partida,
    ));
  }
}

/// @nodoc

class _$Ronda1 implements Ronda1 {
  const _$Ronda1(this.partida);

  @override
  final Partida partida;

  @override
  String toString() {
    return 'Estado2.ron1(partida: $partida)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is Ronda1 &&
            const DeepCollectionEquality().equals(other.partida, partida));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(partida));

  @JsonKey(ignore: true)
  @override
  $Ronda1CopyWith<Ronda1> get copyWith =>
      _$Ronda1CopyWithImpl<Ronda1>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return ron1(partida);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return ron1?.call(partida);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if (ron1 != null) {
      return ron1(partida);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return ron1(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return ron1?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if (ron1 != null) {
      return ron1(this);
    }
    return orElse();
  }
}

abstract class Ronda1 implements Estado2 {
  const factory Ronda1(Partida partida) = _$Ronda1;

  Partida get partida;
  @JsonKey(ignore: true)
  $Ronda1CopyWith<Ronda1> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $Ronda2CopyWith<$Res> {
  factory $Ronda2CopyWith(Ronda2 value, $Res Function(Ronda2) then) =
      _$Ronda2CopyWithImpl<$Res>;
  $Res call({Partida partida});
}

/// @nodoc
class _$Ronda2CopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $Ronda2CopyWith<$Res> {
  _$Ronda2CopyWithImpl(Ronda2 _value, $Res Function(Ronda2) _then)
      : super(_value, (v) => _then(v as Ronda2));

  @override
  Ronda2 get _value => super._value as Ronda2;

  @override
  $Res call({
    Object? partida = freezed,
  }) {
    return _then(Ronda2(
      partida == freezed
          ? _value.partida
          : partida // ignore: cast_nullable_to_non_nullable
              as Partida,
    ));
  }
}

/// @nodoc

class _$Ronda2 implements Ronda2 {
  const _$Ronda2(this.partida);

  @override
  final Partida partida;

  @override
  String toString() {
    return 'Estado2.ron2(partida: $partida)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is Ronda2 &&
            const DeepCollectionEquality().equals(other.partida, partida));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(partida));

  @JsonKey(ignore: true)
  @override
  $Ronda2CopyWith<Ronda2> get copyWith =>
      _$Ronda2CopyWithImpl<Ronda2>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return ron2(partida);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return ron2?.call(partida);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if (ron2 != null) {
      return ron2(partida);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return ron2(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return ron2?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if (ron2 != null) {
      return ron2(this);
    }
    return orElse();
  }
}

abstract class Ronda2 implements Estado2 {
  const factory Ronda2(Partida partida) = _$Ronda2;

  Partida get partida;
  @JsonKey(ignore: true)
  $Ronda2CopyWith<Ronda2> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $Ronda3CopyWith<$Res> {
  factory $Ronda3CopyWith(Ronda3 value, $Res Function(Ronda3) then) =
      _$Ronda3CopyWithImpl<$Res>;
  $Res call({Partida partida});
}

/// @nodoc
class _$Ronda3CopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $Ronda3CopyWith<$Res> {
  _$Ronda3CopyWithImpl(Ronda3 _value, $Res Function(Ronda3) _then)
      : super(_value, (v) => _then(v as Ronda3));

  @override
  Ronda3 get _value => super._value as Ronda3;

  @override
  $Res call({
    Object? partida = freezed,
  }) {
    return _then(Ronda3(
      partida == freezed
          ? _value.partida
          : partida // ignore: cast_nullable_to_non_nullable
              as Partida,
    ));
  }
}

/// @nodoc

class _$Ronda3 implements Ronda3 {
  const _$Ronda3(this.partida);

  @override
  final Partida partida;

  @override
  String toString() {
    return 'Estado2.ron3(partida: $partida)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is Ronda3 &&
            const DeepCollectionEquality().equals(other.partida, partida));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(partida));

  @JsonKey(ignore: true)
  @override
  $Ronda3CopyWith<Ronda3> get copyWith =>
      _$Ronda3CopyWithImpl<Ronda3>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return ron3(partida);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return ron3?.call(partida);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if (ron3 != null) {
      return ron3(partida);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return ron3(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return ron3?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if (ron3 != null) {
      return ron3(this);
    }
    return orElse();
  }
}

abstract class Ronda3 implements Estado2 {
  const factory Ronda3(Partida partida) = _$Ronda3;

  Partida get partida;
  @JsonKey(ignore: true)
  $Ronda3CopyWith<Ronda3> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ObtenerListasCopyWith<$Res> {
  factory $ObtenerListasCopyWith(
          ObtenerListas value, $Res Function(ObtenerListas) then) =
      _$ObtenerListasCopyWithImpl<$Res>;
  $Res call({Usuario usuario});
}

/// @nodoc
class _$ObtenerListasCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $ObtenerListasCopyWith<$Res> {
  _$ObtenerListasCopyWithImpl(
      ObtenerListas _value, $Res Function(ObtenerListas) _then)
      : super(_value, (v) => _then(v as ObtenerListas));

  @override
  ObtenerListas get _value => super._value as ObtenerListas;

  @override
  $Res call({
    Object? usuario = freezed,
  }) {
    return _then(ObtenerListas(
      usuario == freezed
          ? _value.usuario
          : usuario // ignore: cast_nullable_to_non_nullable
              as Usuario,
    ));
  }
}

/// @nodoc

class _$ObtenerListas implements ObtenerListas {
  const _$ObtenerListas(this.usuario);

  @override
  final Usuario usuario;

  @override
  String toString() {
    return 'Estado2.verL(usuario: $usuario)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ObtenerListas &&
            const DeepCollectionEquality().equals(other.usuario, usuario));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(usuario));

  @JsonKey(ignore: true)
  @override
  $ObtenerListasCopyWith<ObtenerListas> get copyWith =>
      _$ObtenerListasCopyWithImpl<ObtenerListas>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return verL(usuario);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return verL?.call(usuario);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if (verL != null) {
      return verL(usuario);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return verL(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return verL?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if (verL != null) {
      return verL(this);
    }
    return orElse();
  }
}

abstract class ObtenerListas implements Estado2 {
  const factory ObtenerListas(Usuario usuario) = _$ObtenerListas;

  Usuario get usuario;
  @JsonKey(ignore: true)
  $ObtenerListasCopyWith<ObtenerListas> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DetallesPartidaCopyWith<$Res> {
  factory $DetallesPartidaCopyWith(
          DetallesPartida value, $Res Function(DetallesPartida) then) =
      _$DetallesPartidaCopyWithImpl<$Res>;
  $Res call({Partida partida, Usuario usuario});
}

/// @nodoc
class _$DetallesPartidaCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $DetallesPartidaCopyWith<$Res> {
  _$DetallesPartidaCopyWithImpl(
      DetallesPartida _value, $Res Function(DetallesPartida) _then)
      : super(_value, (v) => _then(v as DetallesPartida));

  @override
  DetallesPartida get _value => super._value as DetallesPartida;

  @override
  $Res call({
    Object? partida = freezed,
    Object? usuario = freezed,
  }) {
    return _then(DetallesPartida(
      partida == freezed
          ? _value.partida
          : partida // ignore: cast_nullable_to_non_nullable
              as Partida,
      usuario == freezed
          ? _value.usuario
          : usuario // ignore: cast_nullable_to_non_nullable
              as Usuario,
    ));
  }
}

/// @nodoc

class _$DetallesPartida implements DetallesPartida {
  const _$DetallesPartida(this.partida, this.usuario);

  @override
  final Partida partida;
  @override
  final Usuario usuario;

  @override
  String toString() {
    return 'Estado2.verD(partida: $partida, usuario: $usuario)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is DetallesPartida &&
            const DeepCollectionEquality().equals(other.partida, partida) &&
            const DeepCollectionEquality().equals(other.usuario, usuario));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(partida),
      const DeepCollectionEquality().hash(usuario));

  @JsonKey(ignore: true)
  @override
  $DetallesPartidaCopyWith<DetallesPartida> get copyWith =>
      _$DetallesPartidaCopyWithImpl<DetallesPartida>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function() nReg,
    required TResult Function() conB,
    required TResult Function() desc,
    required TResult Function() agPa,
    required TResult Function(Partida partida) ron1,
    required TResult Function(Partida partida) ron2,
    required TResult Function(Partida partida) ron3,
    required TResult Function(Usuario usuario) verL,
    required TResult Function(Partida partida, Usuario usuario) verD,
  }) {
    return verD(partida, usuario);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
  }) {
    return verD?.call(partida, usuario);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function()? nReg,
    TResult Function()? conB,
    TResult Function()? desc,
    TResult Function()? agPa,
    TResult Function(Partida partida)? ron1,
    TResult Function(Partida partida)? ron2,
    TResult Function(Partida partida)? ron3,
    TResult Function(Usuario usuario)? verL,
    TResult Function(Partida partida, Usuario usuario)? verD,
    required TResult orElse(),
  }) {
    if (verD != null) {
      return verD(partida, usuario);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciando value) $default, {
    required TResult Function(NuevoRegistro value) nReg,
    required TResult Function(ConectadoBDD value) conB,
    required TResult Function(DesconectadoBDD value) desc,
    required TResult Function(AgregarPartida value) agPa,
    required TResult Function(Ronda1 value) ron1,
    required TResult Function(Ronda2 value) ron2,
    required TResult Function(Ronda3 value) ron3,
    required TResult Function(ObtenerListas value) verL,
    required TResult Function(DetallesPartida value) verD,
  }) {
    return verD(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
  }) {
    return verD?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciando value)? $default, {
    TResult Function(NuevoRegistro value)? nReg,
    TResult Function(ConectadoBDD value)? conB,
    TResult Function(DesconectadoBDD value)? desc,
    TResult Function(AgregarPartida value)? agPa,
    TResult Function(Ronda1 value)? ron1,
    TResult Function(Ronda2 value)? ron2,
    TResult Function(Ronda3 value)? ron3,
    TResult Function(ObtenerListas value)? verL,
    TResult Function(DetallesPartida value)? verD,
    required TResult orElse(),
  }) {
    if (verD != null) {
      return verD(this);
    }
    return orElse();
  }
}

abstract class DetallesPartida implements Estado2 {
  const factory DetallesPartida(Partida partida, Usuario usuario) =
      _$DetallesPartida;

  Partida get partida;
  Usuario get usuario;
  @JsonKey(ignore: true)
  $DetallesPartidaCopyWith<DetallesPartida> get copyWith =>
      throw _privateConstructorUsedError;
}
