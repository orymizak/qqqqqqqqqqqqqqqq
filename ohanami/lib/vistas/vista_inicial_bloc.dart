import 'exports/export_inicial.dart';

class VistaInicial extends StatefulWidget {
  const VistaInicial({Key? key}) : super(key: key);

  @override
  _VistaInicialState createState() => _VistaInicialState();
}

class _VistaInicialState extends State<VistaInicial> {
  @override
  Widget build(BuildContext context) {
    final estado = context.watch<OhanamiBloc>().state;
    return estado.map(
      // VISTAS
      (_) => const VistaIniciando(),
      
      agPa:  (_) => const NuevaPartida(),
      conB:  (_) => const VistaConectado(),
      nReg:  (_) => const VistaRegistro(),
      desc:  (_) => const VistaDesconectado(),
      ron1:  (r1) => VistaRonda1(partida: r1.partida),
      ron2:  (r2) => VistaRonda2(partida: r2.partida),
      ron3:  (r3) => VistaRonda3(partida: r3.partida),
      verL:  (lis) => ListaPartida(usuario: lis.usuario),
      verD:  (part) => DetallePartida(partida: part.partida, usuario: part.usuario),
    );
  }
}
