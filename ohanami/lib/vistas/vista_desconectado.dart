import 'package:ohanami/vistas/exports/export_inicial.dart';
import 'package:ohanami/vistas/exports/exports.dart';
import 'package:flutter_sesion/flutter_session.dart';

class VistaDesconectado extends StatefulWidget {
  const VistaDesconectado({Key? key}) : super(key: key);

  @override
  _VistaDesconectadoState createState() => _VistaDesconectadoState();
}

class _VistaDesconectadoState extends State<VistaDesconectado> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(left: 35, right: 35, bottom: 150),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            logo,
            espacio,
            espacio,
            Text(
              "No se ha podido establecer conexión con el servidor."
              " Usted puede reintentar, o bien, conectarse localmente.",
              style: titulos5z,
            ),
            espacio,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  child: const Text("Reintentar"),
                  onPressed: () {
                    setState(() {
                      btnConectarse = false;
                    });
                    context.read<OhanamiBloc>().add(VerificarUsuario());
                  },
                ),
                ElevatedButton(
                  child: const Text("Conectarse localmente"),
                  onPressed: () async {
                    Usuario datos = Usuario(
                        usuario: "usuario",
                        telefono: 0,
                        password: "",
                        partidas: []);
                    if (await usuarioLocal.registrarUsuario(usuario: datos) ==
                        true) {
                      await FlutterSession().set("usuario", datos.usuario);
                      await FlutterSession().set("datos", datos);
                      await FlutterSession().set("tipoConexion", 0);
                      context.read<OhanamiBloc>().add(MostrarListas(datos));
                      return;
                    }
                    return widget.toast(context,
                        "Error: no se pudo crear usuario local", Colors.red);
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
