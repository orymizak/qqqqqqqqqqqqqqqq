import 'dart:convert';
import 'package:db_paquete/db_paquete.dart';
import 'package:flutter_sesion/flutter_session.dart';
import 'package:partida/src/partida.dart';

class RepositorioLocal {
  // flutter check
  Future<bool> registrarUsuario({required Usuario usuario}) async {
    await FlutterSession().set("usuario", usuario.usuario);
    await FlutterSession().set("tipoConexion", 0);
    return await FlutterSession().set("datos", usuario).then((value) => true);
  }

  // flutter check
  Future<Usuario> actualizarNombre(String nombre) async {
    await FlutterSession().set("usuario", nombre);
    Usuario usuario = Usuario.fromJson(await FlutterSession().get("datos"));
    Usuario us = Usuario(
        usuario: nombre, telefono: 0, password: "", partidas: usuario.partidas);
    await FlutterSession().set("datos", us).then((value) => true);
    return us;
  }

  // flutter check
  Future<Usuario> registrarPartida({required Partida partida}) async {
    Usuario usuario = Usuario.fromJson(await FlutterSession().get("datos"));
    usuario.partidas.add(partida);
    await FlutterSession().set("datos", usuario).then((value) => true);
    return usuario;
  }

  // flutter check
  Future<bool> eliminarPartida({required int indice}) async {
    Usuario usuario = Usuario.fromJson(await FlutterSession().get("datos"));
    usuario.partidas.removeAt(indice);
    return await FlutterSession().set("datos", jsonEncode(usuario.toJson()));
  }

  // flutter check
  Future<Usuario> recuperarUsuario() async {
    return Usuario.fromJson(await FlutterSession().get("datos"));
  }
}
