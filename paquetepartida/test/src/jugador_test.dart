import 'package:partida/partida.dart';
import 'package:partida/src/problemas.dart';
import 'package:test/test.dart';

void main() {
  group('Jugador', () {
    test('debe de tener nombre no vacío', () {
      expect(() => Jugador(nombre: ''), throwsA(TypeMatcher<ProblemaJugadorNombreVacio>()));
    });
    test('mismo nombre, mismo id', () {
      Jugador j1 = Jugador(nombre: 'Paco');
      Jugador j2 = Jugador(nombre: 'Paco');
      expect(j1, equals(j2));
    });
    test('diferente nombre, diferentes instancias', () {
      Jugador j1 = Jugador(nombre: 'Paco');
      Jugador j2 = Jugador(nombre: 'Juan');
      expect(j1, isNot(equals(j2)));
    });
  });
}
