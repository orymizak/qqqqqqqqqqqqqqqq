import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:partida/partida.dart';
import 'package:partida/src/helpers.dart';
import 'package:partida/src/puntuacion_jugador.dart';

import 'puntuaciones.dart';

const minJugadores = 2;
const maxJugadores = 4;

const maximoCartasRonda2 = 20;
const maximoCartasRonda3 = 30;

const puntosPorAzules = 3;
const puntosPorVerdes = 4;
const puntosPorNegras = 7;
  var puntosPorRosas = {0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120}.toList();

enum FasePuntuacion { ronda1, ronda2, ronda3, desenlace }

class Partida {

  final Set<Jugador> jugadores;
  List<CartasRonda1> puntuacionesRonda1 = [];
  List<CartasRonda2> puntuacionesRonda2 = [];
  List<CartasRonda3> puntuacionesRonda3 = [];
  DateTime fecha = DateTime.now();
  Partida({
    required this.jugadores,
  }){
    if (jugadores.length < minJugadores) throw ProblemaNumeroJugadoresMinimo();
    if (jugadores.length > maxJugadores) throw ProblemaNumeroJugadoresMaximo();
  }

  Partida.constructor({
    required this.jugadores,
    required this.puntuacionesRonda1,
    required this.puntuacionesRonda2,
    required this.puntuacionesRonda3,
    required this.fecha,
  });

  List<PuntuacionJugador> puntos({required FasePuntuacion ronda}) {
    List<PuntuacionJugador> _pj = [];
    switch (ronda) {
      case FasePuntuacion.ronda1:
        for (var i in puntuacionesRonda1) {
          _pj.add(PuntuacionJugador(
            jugador: i.jugador,
            porAzules: i.cuantasAzules * puntosPorAzules,
            porVerdes: 0,
            porRosas: 0,
            porNegras: 0,
          ));
        }
        return _pj;

      case FasePuntuacion.ronda2:
        for (var i in puntuacionesRonda2) {
          _pj.add(PuntuacionJugador(
            jugador: i.jugador,
            porAzules: i.cuantasAzules * puntosPorAzules,
            porVerdes: i.cuantasVerdes * puntosPorVerdes,
            porRosas: 0,
            porNegras: 0,
          ));
        }
        return _pj;

      case FasePuntuacion.ronda3:
        for (var i in puntuacionesRonda3) {
          _pj.add(PuntuacionJugador(
            jugador: i.jugador,
            porAzules: i.cuantasAzules * puntosPorAzules,
            porVerdes: i.cuantasVerdes * puntosPorVerdes,
            porRosas: i.cuantasRosas > 15 ? 120 : puntosPorRosas[i.cuantasRosas],
            porNegras: i.cuantasNegras * puntosPorNegras,
          ));
        }

        return _pj;
      case FasePuntuacion.desenlace:
        for (Jugador j in jugadores) {
          int azules = puntos(ronda: FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porAzules +
              puntos(ronda: FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porAzules +
              puntos(ronda: FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porAzules;

          int verdes = puntos(ronda: FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porVerdes +
              puntos(ronda: FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porVerdes +
              puntos(ronda: FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porVerdes;

          int rosas = puntos(ronda: FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porRosas +
              puntos(ronda: FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porRosas +
              puntos(ronda: FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porRosas;

          int negras = puntos(ronda: FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porNegras +
              puntos(ronda: FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porNegras +
              puntos(ronda: FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porNegras;

          _pj.add(PuntuacionJugador(
              jugador: j,
              porAzules: azules,
              porVerdes: verdes,
              porRosas: rosas,
              porNegras: negras));
        }
        return _pj;
    }
  }

  void cartasronda1(List<CartasRonda1> puntuaciones) {
    Set<Jugador> jugadoresR1 = puntuaciones.map((e) => e.jugador).toSet();
    if (!setEquals(jugadores, jugadoresR1)) throw ProblemaJugadoresInconsistentes();
    puntuacionesRonda1 = puntuaciones;
  }
  void cartasronda2(List<CartasRonda2> puntuaciones) {
    if (puntuacionesRonda1.isEmpty) throw ProblemaOrdenIncorrecto();

    Set<Jugador> jugadoresR2 = puntuaciones.map((e) => e.jugador).toSet();
    if (!setEquals(jugadores, jugadoresR2)) throw ProblemaJugadoresInconsistentes();

    for (CartasRonda2 segundaPuntuacion in puntuaciones) {
      CartasRonda1 primeraPuntuacion = puntuacionesRonda1.firstWhere(
          (element) => element.jugador == segundaPuntuacion.jugador);
      if (primeraPuntuacion.cuantasAzules > segundaPuntuacion.cuantasAzules) throw ProblemaDisminucionAzules();
    }

    for (CartasRonda2 p in puntuaciones) {
      if (p.cuantasAzules > maximoCartasRonda2) throw ProblemaDemasiadasAzules();
      if (p.cuantasVerdes > maximoCartasRonda2) throw ProblemaDemasiadasVerdes();
      if ((p.cuantasAzules + p.cuantasVerdes) > maximoCartasRonda2) throw ProblemaExcesoCartas();
    }

    puntuacionesRonda2 = puntuaciones;
  }
  void cartasronda3(List<CartasRonda3> puntuaciones) {
    if (puntuacionesRonda2.isEmpty) throw ProblemaOrdenIncorrecto();

    Set<Jugador> jugadoresR3 = puntuaciones.map((e) => e.jugador).toSet();
    if (!setEquals(jugadores, jugadoresR3)) throw ProblemaJugadoresInconsistentes();

    for (CartasRonda3 terceraPuntuacion in puntuaciones) {
      CartasRonda2 segundaPuntuacion = puntuacionesRonda2.firstWhere(
          (element) => element.jugador == terceraPuntuacion.jugador);
      if (segundaPuntuacion.cuantasAzules > terceraPuntuacion.cuantasAzules) throw ProblemaDisminucionAzules();
      if (segundaPuntuacion.cuantasVerdes > terceraPuntuacion.cuantasVerdes) throw ProblemaDisminucionVerdes();
    }
    for (CartasRonda3 p in puntuaciones) {
      if (p.cuantasAzules > maximoCartasRonda3) throw ProblemaDemasiadasAzules();
      if (p.cuantasVerdes > maximoCartasRonda3) throw ProblemaDemasiadasVerdes();
      if (p.cuantasRosas > maximoCartasRonda3) throw ProblemaDemasiadasRosas();
      if (p.cuantasNegras > maximoCartasRonda3) throw ProblemaDemasiadasNegras();
      if ((p.cuantasAzules +
              p.cuantasVerdes +
              p.cuantasRosas +
              p.cuantasNegras) >
          maximoCartasRonda3) throw ProblemaExcesoCartas();
    }
    puntuacionesRonda3 = puntuaciones;
  }

  Partida copyWith({
    Set<Jugador>? jugadores,
    List<CartasRonda1>? puntuacionesRonda1,
    List<CartasRonda2>? puntuacionesRonda2,
    List<CartasRonda3>? puntuacionesRonda3,
    DateTime? fecha,
  }) {
    return Partida.constructor(
      jugadores: jugadores ?? this.jugadores,
      puntuacionesRonda1: puntuacionesRonda1 ?? this.puntuacionesRonda1,
      puntuacionesRonda2: puntuacionesRonda2 ?? this.puntuacionesRonda2,
      puntuacionesRonda3: puntuacionesRonda3 ?? this.puntuacionesRonda3,
      fecha: fecha ?? this.fecha,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jugadores': jugadores.map((x) => x.toMap()).toList(),
      'puntuacionesRonda1': puntuacionesRonda1.map((x) => x.toMap()).toList(),
      'puntuacionesRonda2': puntuacionesRonda2.map((x) => x.toMap()).toList(),
      'puntuacionesRonda3': puntuacionesRonda3.map((x) => x.toMap()).toList(),
      'fecha': fecha.millisecondsSinceEpoch,
    };
  }

  factory Partida.fromMap(Map<String, dynamic> map) {
    return Partida.constructor(
      jugadores: Set<Jugador>.from(map['jugadores']?.map((x) => Jugador.fromMap(x))),
      puntuacionesRonda1: List<CartasRonda1>.from(map['puntuacionesRonda1']?.map((x) => CartasRonda1.fromMap(x))),
      puntuacionesRonda2: List<CartasRonda2>.from(map['puntuacionesRonda2']?.map((x) => CartasRonda2.fromMap(x))),
      puntuacionesRonda3: List<CartasRonda3>.from(map['puntuacionesRonda3']?.map((x) => CartasRonda3.fromMap(x))),
      fecha: DateTime.fromMillisecondsSinceEpoch(map['fecha']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Partida.fromJson(String source) => Partida.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Partida(jugadores: $jugadores, puntuacionesRonda1: $puntuacionesRonda1, puntuacionesRonda2: $puntuacionesRonda2, puntuacionesRonda3: $puntuacionesRonda3, fecha: $fecha)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final collectionEquals = const DeepCollectionEquality().equals;
  
    return other is Partida &&
      collectionEquals(other.jugadores, jugadores) &&
      collectionEquals(other.puntuacionesRonda1, puntuacionesRonda1) &&
      collectionEquals(other.puntuacionesRonda2, puntuacionesRonda2) &&
      collectionEquals(other.puntuacionesRonda3, puntuacionesRonda3) &&
      other.fecha == fecha;
  }

  @override
  int get hashCode {
    return jugadores.hashCode ^
      puntuacionesRonda1.hashCode ^
      puntuacionesRonda2.hashCode ^
      puntuacionesRonda3.hashCode ^
      fecha.hashCode;
  }
}
